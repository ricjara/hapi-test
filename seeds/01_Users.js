
exports.seed = function(knex, Promise) {

    var tableName = 'users';
    
    var rows = [
        {
            username: 'testuser',
            password: '1234',
            created_at: knex.raw('now()'),
        },
    ];

    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(function () {
        // Inserts seed entries
        return knex(tableName).insert(rows);
    });
};
