
exports.seed = function(knex, Promise) {

    var tableName = 'posts';
    
    var rows = [
        {
            owner_id: 3,
            content: 'Hello, this is a test post which is not a lorem ipsum.',
            created_at: knex.raw('now()'),
        },
        {
            owner_id: 3,
            content: 'Sup, dawg.',
            created_at: knex.raw('now()'),
        },
    ];

    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(function () {
        // Inserts seed entries
        return knex(tableName).insert(rows);
    });
};
