module.exports = {
    development: {
        migrations: { tableName: 'knex]_migrations' },
        seeds: { tableName: './seeds' },
        
        client: 'pg',
        connection: {
            host: '127.0.0.1',
            user: 'hapitest',
            password: 'hapi123',
            database: 'hapitest',
            charset: 'utf8'
        }
    }
};