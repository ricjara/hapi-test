
exports.up = function(knex, Promise) {
  
    return knex
        .schema
        .createTable( 'users', function( usersTable ) {
            // PK
            usersTable.increments();

            // Data
            usersTable.string('username', 50).notNullable().unique();
            usersTable.string('password', 128).notNullable();

            usersTable.timestamp('created_at').notNullable();
        })
        .createTable( 'posts', function( postsTable ) {
            // PK
            postsTable.increments();
            // FK
            postsTable.integer('owner_id').unsigned();
            postsTable.foreign('owner_id').references('id').inTable('users');
            
            // Data
            postsTable.string('content', 250).notNullable();
            
            postsTable.timestamp('created_at').notNullable();
        });

};

exports.down = function(knex, Promise) {

    return knex
        .schema
            .dropTableIfExists('posts')
            .dropTableIfExists('users');

};
