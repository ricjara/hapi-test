import Hapi from 'hapi';
import Knex from './knex';
import jwt from 'jsonwebtoken';

const server = new Hapi.Server();

server.connection( {
    host: '127.0.0.1',
    port: 8080
});

server.register( require('hapi-auth-jwt'), (err) => {
    server.auth.strategy('token','jwt', {
        key: 'bsTSDV4uyeAYE4QETlk2jvMVSN8fgaZx9xxCCCv',
        
        verifyOptions: {
            algorithms: ['HS256'],
        }
    });
});

server.start(err => {
    if(err) {
        console.error('Error handled');
        console.error(err);
    }
    
    console.log(`Server -> ${ server.info.uri }`);
});


///////////////////
//     ROUTES    //
///////////////////
server.route({
    path: '/posts',
    method: 'GET',
    handler: (request, reply) => {
        const getOperation = Knex('posts')
            .select('content').then ((results) =>{
                if(!results || results.length === 0) {
                    reply({
                        error: true,
                        errMessage: 'No posts found'
                    });
                }

                reply({
                    dataCount: results.length,
                    data: results
                });
            }).catch((err) => {
                reply('Server error');
            });
    }
});

server.route({
    path: '/auth',
    method: 'POST',
    handler: (request,reply) => {
        const {username, password} = request.payload; // ES6 equivalente a const username = request.payload.username; const password = ...
        const getOperation = Knex('users').where({
            username
        }).select('id', 'password').then(([user]) => {
            if(!user) {
                reply({
                    error: true,
                    errMessage: 'User not found'
                });
                return;
            }
            if(user.password === password) { // INSEGURO, ME DIO PAJA HASHEAR
                const token = jwt.sign({
                    username,
                    scope: user.id,
                }, 'bsTSDV4uyeAYE4QETlk2jvMVSN8fgaZx9xxCCCv', {
                    algorithm: 'HS256'
                });
                
                reply({
                    token,
                    scope: user.id
                });
            } else {
                reply('Wrong password');
            }
        }).catch((err) => {
            reply('Server error');
        });
    }
});